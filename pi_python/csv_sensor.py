#!/usr/bin/env python
# -*- coding: utf8 -*-
import serial
import sys
from datetime import datetime
from shutil import copyfile
i = 1 # Wert dient zur Kontrolle der Schleife unten
ser = serial.Serial('/dev/ttyUSB0', 9600) # Serielle Schnittstelle definieren
ser.readline() # 1x lesen ohne zu Speichern - initialisiert das Interface
output = "" # String Variable für die CSV vorbereiten
now = datetime.now() # Zeitstempel der Aktuellen Systemzeit holen
filename = "/home/pi/ballon/outputs/" # Pfad für CSV Datei ablegen
filename += now.strftime("%d-%m-%Y_%H-%M-%S") # An Pfad Zeitstempel anhängen - like: 01-01-1970_12-23-32
filename += ".csv" # Pfad um Dateiendung ergänzen
ofh = open(filename, "w") # Datei auf Dateisystem zum Schreiben öffnen
# kommende Anweisung Schreibt den Header in die CSV, woran man erkennt, welche spalte, welchen wert Darstellt
ofh.write("Time,GM Counter,uv0.volt,uv0.index,uv1.volt,uv1.index,uv2.volt,uv2.index,Temprature.Temp,Pressure.TempRAW,Pressure.PressureRAW,Pressure.Temp,Pressure.Pressure\n")
ofh.close() # Datei aus sicherheitsgründen wieder Schließen
while i == 1:
	line = ser.readline() # lies einen Text vom Arduino via Serielle Schnittstelle
	line = line[ : len(line)-2] # Entferne nicht gewünschte Endung der Übertragung
	if line == "--":# Arduino sagt - Ende der Werte übermittlung
		copyfile(filename, filename + ".safety")# Sicherheitskopie der letzten Dateiversion
		ofh = open(filename, "a") # öffne CSV im Anfüge Modus
		ts = datetime.now()
		ts_str = ts.strftime("%d.%m.%Y %H:%M:%S") # Konvertiere Zeitstempel - like: 01.01.1970 12:23:32
		ofh.write(ts_str)# Schreibe zuerst den Zeitstempel
		output += '\n' # ergänze Werte Liste um Zeilenumbruch (Neue Tabellen Zeile)
		ofh.write(output) # Schreibe Werte Liste in CSV
		ofh.close() # schließe die Datei, damit nichts passiert
		output = "" # leere output wieder, damit es von vorne losgehen kann
	elif line.endswith(":"): # Aduino hat eine Prefix Zeile gesendet die soll nicht in die Datei (nur für Serial Monitor gedacht)
		print("Skipped line")# this code line is need - elif to handle prefix outputs - print bec. there must be code	
	elif line.endswith("XX"): # Arduino teilt mit dass das Kill Switch Signal gegeben wurde
		i = 0 # Bewirke ende Der schleife
	else: # Es wurde Eine Zahl für einen wert gesendet
		output += ',' + line # Zahl wird angehangen

ser.close() # bei Erfolgreichem ende des Programms Serielle Schnittstelle schließen
