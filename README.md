# RHG Strato I (2019)
## With the Raspberry Pi into the Stratosphere

In 2019 the pupil at Robert-Havermann-Gymnasium in Berlin wanted to start a ballon into the stratosphere. 
There the wanted to make measurements of physical values like airpreassure, radio activity, ultraviolet radiation and temprature.
Also a GPS tracker was onboard. The maximum flight hight was 40.1km. A camera maked beautiful pictures.

The system and the software was made by me and others of my studium. As Hardware we connect all sensors to an Arduino and this by USB to a Raspberry Pi. The Pi logs all data into a CSV File and also takes some photos with a Pi Camera. See more about the project (and some pictures) at [My Website / Hardware / RHG](http://pm-sys.de/hw/rhg.php).

Here in this repo I stored the original code of the Arduino and the Raspberry Pi. The Arduino Code includes two ZIP files for libaries which had to be installed with the IDE. 
