#include <DS18B20.h>
#include <OneWire.h>

/*
MS5541 Pressure Sensor calwords readout
This program will read your MS5441 or compatible pressure sensor every 5 seconds and show you the calibration words, the calibration factors, 
the raw values and the compensated values of temperature and pressure.
Once you read out the calibration factors you can define them in the header of any sketch you write for the sensor.

Pins:
MS5541 sensor attached to pins 10 - 13:
MOSI: pin 11
MISO: pin 12
SCK: pin 13
MCLK: pin 9
CS is not in use, but might be pin 10

created August 2011
by SMStrauch based on application note AN510 from www.intersema.ch (http://www.meas-spec.com/downloads/Using_SPI_Protocol_with_Pressure_Sensor_Modules.pdf), 
and with help of robtillaart and ulrichard. Thanks!
*/

// include library:
#include <SPI.h>

// generate a MCKL signal pin
const int clock = 9;

int uvSensorIn1 = A5;
int uvSensorIn2 = A4;
int uvSensorIn3 = A3;
#define ONEWIRE_PIN 5
byte address[8] = {0x28, 0xAA, 0x44, 0x25, 0x3D, 0x14, 0x01, 0xCD};
OneWire onewire(ONEWIRE_PIN);
DS18B20 sensors(&onewire);

void SafeSendValue(String value){
  noInterrupts();
  Serial.println(value);
  interrupts();
}

void TempSetup() {
  sensors.begin();
  sensors.request(address);
}

void TempLoop() {
  if (sensors.available())
  {
    float temperature = sensors.readTemperature(address);
    SafeSendValue("Temp:");
    SafeSendValue(String(temperature));
    sensors.request(address);
  }
}
//GMZ
#define LOG_PERIOD 60000 // count rate (GMZ)
unsigned long counts;
unsigned long currMillis;
unsigned long prevMillis;


int UVindexByMV(int mv){
 if (mv < 227){
  return 0;
 } else if ((mv >= 227) && (mv < 318)){
  return 1; 
 } else if ((mv >= 318) && (mv < 408)){
  return 2; 
 } else if ((mv >= 408) && (mv < 503)){
  return 3;
 } else if ((mv >= 503) && (mv < 606)){
  return 4;
 } else if ((mv >= 606) && (mv < 696)){
   return 5;
 } else if ((mv >= 696) && (mv < 795)){
   return 6;
 } else if ((mv >= 795) && (mv < 881)){
   return 7;
 } else if ((mv >= 881) && (mv < 976)){
   return 8;
 } else if ((mv >= 976) && (mv < 1070)){
   return 9;
 } else {
   return 10;
 }
}

//GMZ
void impulse(){
  counts++;
}

void UVsetup(){
  pinMode(uvSensorIn1, INPUT); 
  pinMode(uvSensorIn2, INPUT);
  pinMode(uvSensorIn3, INPUT);
  analogReference(INTERNAL);
}

void UVloop(){
  const int sensors[3] = {uvSensorIn1, uvSensorIn2, uvSensorIn3};
 for (unsigned int i = 0; i < 3; i++){
  int uvValue = analogRead(sensors[i]);
  String prefix = "UV";
  prefix += (i+1);
  prefix += ":";
  prefix += ".volt:";
  SafeSendValue(prefix);
  SafeSendValue(String(uvValue));
  prefix = "UV";
  prefix += (i+1);
  prefix += ".index:";
  SafeSendValue(prefix);
  SafeSendValue(String(UVindexByMV(uvValue)));
 } 
} 


void gMZsetup() {
  counts = 0;
  prevMillis = 0;
  pinMode(2, INPUT);
  attachInterrupt(digitalPinToInterrupt(2), impulse, FALLING);//aktiv wenn Flanke fällt. zählt counts +1
}

void gMZloop() {
  int run = 1;
  while(run){
  currMillis = millis();
  if( LOG_PERIOD <= (currMillis - prevMillis) && currMillis >= 1){
    prevMillis = currMillis;
    SafeSendValue("gmc:");
    SafeSendValue(String(counts));
    counts = 0;
    run = 0;
  }
   }
}

void resetsensor() //this function keeps the sketch a little shorter
{
 SPI.setDataMode(SPI_MODE0); 
 SPI.transfer(0x15);
 SPI.transfer(0x55);
 SPI.transfer(0x40);
}

void PressureSetup() {
 SPI.begin(); //see SPI library details on arduino.cc for details
 SPI.setBitOrder(MSBFIRST);
 SPI.setClockDivider(SPI_CLOCK_DIV32); //divide 16 MHz to communicate on 500 kHz
 pinMode(clock, OUTPUT);
 delay(100);
}

void PressureLoop() 
{
 TCCR1B = (TCCR1B & 0xF8) | 1 ; //generates the MCKL signal
  analogWrite (clock, 128) ;

  long c1 = 22720;
  long c2 = 1912;
  long c3 = 716;
  long c4 = 612;
  long c5 = 1011;
  long c6 = 23;

  resetsensor();//resets the sensor

  //Temperature:
  unsigned int tempMSB = 0; //first byte of value
  unsigned int tempLSB = 0; //last byte of value
  unsigned int D2 = 0;
  SPI.transfer(0x0F); //send first byte of command to get temperature value
  SPI.transfer(0x20); //send second byte of command to get temperature value
  delay(35); //wait for conversion end
  SPI.setDataMode(SPI_MODE1); //change mode in order to listen
  tempMSB = SPI.transfer(0x00); //send dummy byte to read first byte of value
  tempMSB = tempMSB << 8; //shift first byte
  tempLSB = SPI.transfer(0x00); //send dummy byte to read second byte of value
  D2 = tempMSB | tempLSB; //combine first and second byte of value
  SafeSendValue("pressureTemperature raw:");
  SafeSendValue(String(D2)); //voilá!

  resetsensor();//resets the sensor

  //Pressure:
  unsigned int presMSB = 0; //first byte of value
  unsigned int presLSB =0; //last byte of value
  unsigned int D1 = 0;
  SPI.transfer(0x0F); //send first byte of command to get pressure value
  SPI.transfer(0x40); //send second byte of command to get pressure value
  delay(35); //wait for conversion end
  SPI.setDataMode(SPI_MODE1); //change mode in order to listen
  presMSB = SPI.transfer(0x00); //send dummy byte to read first byte of value
  presMSB = presMSB << 8; //shift first byte
  presLSB = SPI.transfer(0x00); //send dummy byte to read second byte of value
  D1 = presMSB | presLSB; //combine first and second byte of value
  SafeSendValue("Pressure raw:");
  SafeSendValue(String(D1));

  //calculation of the real values by means of the calibration factors and the maths
  //in the datasheet. const MUST be long
  const double UT1 = (c5 << 3) + 20224;
  const double dT = D2 - ((8.0 * c5) + 20224.0);
  //const long TEMP = 200 + dT * (c6 + 50) >> 10;
  const double OFF  = c2 * 4.0         + (  (   ( c4-512.0) *  dT ) / 4096.0);
  const double SENS = 24576.0 +  c1    + (  (   c3 *  dT ) / 1024.0);
  const double X = (( SENS * (D1- 7168.0)) / 16384.0) -OFF;
  double PCOMP = 250.0 +   X / 32;    //(SENS * (D1 - OFF) >> 12) + 1000;
  double TEMP = 20.0 +      ( (  dT * ( c6+50.0) ) / 10240.0);
  float TEMPREAL = TEMP/10;
 

  SafeSendValue("pressureTemperature:");
  SafeSendValue(String(TEMP));

  SafeSendValue("Pressure:");
  SafeSendValue(String(PCOMP));
}

void kill(){
  noInterrupts();
  while(1){
    Serial.println("XX");
  }
 }
 
void setup(){
  UVsetup();
  TempSetup();
  gMZsetup();
  PressureSetup();
  pinMode(3, INPUT);
  attachInterrupt(digitalPinToInterrupt(3), kill, LOW);
  Serial.begin(9600);
}

void loop(){
  gMZloop();
  UVloop();
  TempLoop();
  PressureLoop();
  SafeSendValue("--");
}
